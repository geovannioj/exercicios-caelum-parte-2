class Funcionario {
	

	// criando atributos
	private String nome;
	private String departamento;
	private double salario;
	Data dataDeEntrada;
	private String rg;
	private double ganhoAnual;

	void mostra()
		{
			System.out.println("nome:" + this.nome);
			System.out.println("departamento:" + this.departamento);
			System.out.println("salario:"+this.salario);
			System.out.println("dia: " + this.dataDeEntrada.dia);
			System.out.println("Mês: " + this.dataDeEntrada.mes);
			System.out.println("Ano: " + this.dataDeEntrada.ano);
			System.out.println("RG"+ this.rg);
			System.out.println("ganhoAnual"+ this.ganhoAnual);
		}
	//iniciando métodos

	public void setNome(String umNome){
		this.nome = umNome;
	}
	public String getNome (){
		return nome;
	}
	public void setDepartamento (String umDepartamento){
		this.departamento = umDepartamento;
	}
	public String getDepartamento () {
		return departamento;
	}
	public void setSalario(double umSalario){
		this.salario = umSalario;
	}
	public double getSalario(){
		return salario;
	}

	
	
	public void setRg(String umRg){
		this.rg = umRg;
	}

	public String getRg (){
		return rg;
	}

	public void recebeAumento(double valorAumento)
	{
		this.salario = this.salario + valorAumento;
	}

	public void recebeAumento(int porcentagemAumento)
	{
		this.salario = this.salario+(this.salario*(porcentagemAumento/100));
	}

	public double calculaGanhoAnual (double salario)
	{
		ganhoAnual = salario*12;
		return ganhoAnual;
	}
	
}

